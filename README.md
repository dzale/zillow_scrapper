# Zillow scrapper
  //todo add short description

## Installation

1.  Clone source to target machine

        git clone https://dzale@bitbucket.org/dzale/zillow_scrapper.git

2.  Cd to project directory (zillow_scrapper)

3.  You may need to configure some permissions. Directories within the storage 
    and the bootstrap/cache directories should be writable by your web server.

4.  Install composer if not installed already:

        curl -sS https://getcomposer.org/installer | php
        sudo mv composer.phar /usr/local/bin/composer

5.  Install composer dependencies

        composer install

6.  Copy env file

        cp .env.example .env
        
7.  Run migrations and seeders

        php artisan migrate --seed
        
8. Run scrapping command
        
        php artisan scrape:zillow
       
9. Generate key
               
        php artisan key:generate
       
10. Run application (or if you have vhost or vps, then just skip remaining steps and go directly to vhost url)

        php artisan serve

11. Open browser and type "localhost:8000"
