<?php

namespace App\Http\Controllers;

use App\Models\Property;
use App\Tables\PropertiesTable;
use Illuminate\View\View;


class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
        $table = (new PropertiesTable())->setup();

        return view('welcome', compact('table'));
    }


    /**
     * Display the specified resource.
     *
     * @param  Property $property
     * @return \Illuminate\Http\Response
     */
    public function show(Property $property)
    {
        return view('single', compact('property'));
    }

}
