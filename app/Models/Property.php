<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'properties';

    public $timestamps = true;

    protected $fillable = [
        'zpid',
        'region_id',
        'name',
        'address',
        'price',
        'city',
        'state',
        'unit_number',
        'beds',
        'baths',
        'type',
        'cooling',
        'heating',
        'parking',
        'year_built',
        'laundry',
        'listed_at',
        'first_seen_at',
        'last_seen_at',
    ];

    public function prices()
    {
        return $this->hasMany(PropertyPrice::class)->orderBy('created_at', 'desc');
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }
}
