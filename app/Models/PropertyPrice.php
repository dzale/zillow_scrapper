<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyPrice extends Model
{
    protected $table = 'property_prices';

    public $timestamps = true;

    protected $fillable = [
        'proprty_id',
        'price',
    ];

    /**
     * Get formatted date of creating.
     *
     * @return string
     */
    public function getDateAttribute()
    {
        return $this->created_at->format("d-m-'y");
    }

    /**
     * Get formatted date of creating.
     *
     * @return string
     */
    public function getNormalizedPriceAttribute()
    {
        return str_replace(',', '', $this->price);
    }

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    /**
     * Scope a group property prices by date
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDateGrouping($query)
    {
        return $query->select(DB::raw('date(created_at)'), 'price' )->groupBy(DB::raw('date(created_at)'));
    }

}
