<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $table = 'regions';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'url',
    ];

    public function properties()
    {
        return $this->hasMany(Property::class);
    }
}
