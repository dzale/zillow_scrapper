<?php

namespace App\Services\Common\Guzzle\Middleware;

use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class RetryDeciders
{
    public static function standard(int $maxRetries = 3): callable
    {
        return function (
            int $retries,
            Request $request,
            Response $response = null,
            RequestException $exception = null
        ) use ($maxRetries) : bool {
            if ($retries >= $maxRetries) {
                return false;
            }
            if ($exception instanceof ConnectException) {
                return true;
            }
            if ($response && $response->getStatusCode() >= 500) {
                return true;
            }
            return false;
        };
    }
}
