<?php

namespace App\Services\Common\Guzzle\Middleware;
use Illuminate\Support\Facades\Log;
class RetryDelays
{
    public static function linear(): callable
    {
        return function (int $retries): int {
            if (app()->environment() === 'testing') {
                return 0;
            }
            Log::notice('Sleep for 60s!');
            return 60 * 1000 * $retries;
        };
    }
}
