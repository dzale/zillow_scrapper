<?php

namespace App\Services\Common\Guzzle;

use App\Services\Common\Guzzle\Middleware\RetryDeciders;
use App\Services\Common\Guzzle\Middleware\RetryDelays;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\RequestOptions;
use Str;

class ScraperClient
{
    protected function getClient() {
        $client = $this->initClient();
        $client->getConfig('handler')->push(
            Middleware::retry(
                RetryDeciders::standard(3),
                RetryDelays::linear(),
                ),
            );

        return $client;
    }

    /**
     * Predefine get request
     * @param string $url
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function get(string $url) {
        try {
            $this->delay();
            Log::debug("FETCHING: ".$url);
            return $this->getClient()->get($url);
        } catch (\Exception $e) {
            Log::critical($e);
            return null;
        }
        Log::error("SKIPPED: " . $url);
        return null;
    }

    /**
     * Predefine post request
     * @param string $url
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function post(string $url, array $data = []) {
        try {
            $this->delay();
            Log::debug("FETCHING: " . $url);
            return $this->getClient()->post($url,  [ RequestOptions::JSON => $data]);
        } catch (\Exception $e) {
            Log::critical($e);
            return null;
        }
    }

    /**
     * Add random delay between two calls
     */
    private function delay($force = false) {
        if (!$force && config('app.debug')) {
            return;
        }
        $minDelay = config('zillow.min_seconds_delay');
        $maxDelay = config('zillow.max_seconds_delay');
        if ($maxDelay < 1 || $minDelay >= $maxDelay) {
            return;
        }
        $delay = rand($minDelay, $maxDelay);
        Log::info("Sleeping for $delay seconds!");
        sleep($delay);
    }
}
