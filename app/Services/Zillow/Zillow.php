<?php

namespace App\Services\Zillow;

use App\Models\Property;
use App\Models\PropertyPrice;
use App\Models\Region;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Str;

class Zillow
{
    protected $client;
    protected $baseUrl;
    protected $currentPage;
    protected $initialQueryParams;
    protected $totalPages;
    protected $totalResults;
    protected $perPage;
    protected $defaultBuildingInfo;
    protected $backupBuildingInfo;
    protected $buildingData;
    protected $buildingHtml;

    public function __construct()
    {
        $this->initialQueryParams = "";
        $this->baseUrl = config('zillow.search_url');
        $this->resetPagination();
        $this->defaultBuildingInfo = [
            'year_built' => null,
            'sqft' => null,
            'beds' => null,
            'baths' => null,
            'price' => null,
            'img' => null,
            'type' => null,
            'listed_at' => null,
            'heating' => null,
            'cooling' => null,
            'parking' => null,
            'laundry' => null,
        ];
        $this->backupBuildingInfo = $this->defaultBuildingInfo;
        $this->buildingData = null;
        $this->buildingHtml = null;
        $this->client = new ZillowClient();

    }

    /**
     * Fetch all properties for rent based on db regions
     * @param string|null $url
     * @param bool $split
     * @return bool
     */
    public function getPropertiesForRent(string $url = null, bool $split = false)
    {
        $regions = Region::orderBY('attempt')->get();
        foreach ($regions as $key => $region) {
            if($key > 0) {
                $regions[$key-1]->attempt++;
                $regions[$key-1]->save();
            }
            $this->resetPagination();
            if (!$url) {
                $url = $this->nextPageUrl($region->url, false);
            }
            if ($url) {
                $data = $this->getProperties($url);
                if (!$data) {
                    continue;
                }

                //if region is too big we have to split it
                if ($split && $this->totalResults > $this->totalPages * $this->perPage) {
                    $this->splitRegion($url);
                    continue;
                }
                while ($this->currentPage <= $this->totalPages) {
                    foreach ($data->cat1->searchResults->listResults as $key => $item) {
                        $this->processProperty($item, $region);
                    }
                    $url = $this->nextPageUrl($region->url);
                    $data = $this->getProperties($url);
                    if (!$data) {
                        break;
                    }
                }
            }
            $region->attempt++;
            $region->save();
        }

        return true;
    }

    /**
     * Create next page url based on current
     * @param string $url
     * @param bool $increment
     * @return string
     */
    protected function nextPageUrl(string $url, bool $increment = true) : string
    {
        $parts = parse_url($url);
        //$baseUrl = explode( '?searchQueryState', $url)[0];
        //$this->baseUrl = explode( '?searchQueryState', $url)[0];
        parse_str($parts['query'], $this->initialQueryParams);
        if (isset($this->initialQueryParams['searchQueryState'])) {
            $params = json_decode($this->initialQueryParams['searchQueryState'], false);
            if($increment){
                $this->currentPage++;
                if(isset($params->pagination->currentPage)){
                    $params->pagination->currentPage = $this->currentPage;
                } else {
                    $params->pagination = ['currentPage' => $this->currentPage];
                }
            }
            return $this->baseUrl . json_encode($params);
        }
        return '';
    }

    /**
     * Fetch all properties for given url
     * @param string $url
     * @return mixed|string|null
     */
    protected function getProperties(string $url)
    {
        try {
            if ($url) {
                $response = $this->client->post($url);
                if ($response && $response->getBody()) {
                    $html = $response->getBody()->getContents();

                    if (!$html) {
                        Log::error('getProperties: EMPTY RESPONSE DETECTED!');
                        return null;
                    }
                    preg_match_all('/\><!--{"queryState"(.*?)}}--><\/script>/', $html, $matches);

                    $totalResultCount = 0;
                    $data = null;
                    foreach ($matches[0] as $match) {
                        $string = str_replace('><!--', '', $match);
                        $string = str_replace('--></script>', '', $string);
                        $object = json_decode($string,false);

                        if ($object->cat1 && $object->cat1->searchList
                            && $object->cat1->searchList->totalResultCount
                            && $object->cat1->searchList->totalResultCount > $totalResultCount)
                        {
                            $data = $object;
                        }
                    }

                    if (!isset($data->cat1) && !isset($data->cat1->searchList)) {
                        if(strpos($html, 'Captcha') !== false) {
                            Log::error('getProperties: CAPTCHA!');
                        } else {
                            Log::error('getProperties: NO CAT1->SEARCH LIST!');
                        }
                        return null;
                    }
                    $this->totalPages = $data->cat1->searchList->totalPages;
                    $this->totalResults = $data->cat1->searchList->totalResultCount;
                    $this->perPage = $data->cat1->searchList->resultsPerPage;
                    return $data;
                } else {
                    Log::error('getProperties: INVALID URL!');
                    return null;
                }
            }
        } catch (\Exception $e) {
            Log::critical($e);
        }
        return null;
    }

    /**
     * Fetch units for property
     * @param string $url
     * @return |null
     */
    protected function getUnits(string $url)
    {
        try {
            if ($url) {
                $response = $this->client->post($url);
                if ($response && $response->getBody()) {
                    $singleUnitData = $response->getBody()->getContents();
                    //printf('getUnits'.PHP_EOL . $url);
                    return $this->parseUnits($singleUnitData);
                }
            } else {
                return null;
            }
        } catch (\Exception $e) {
            Log::critical($e);
        }
        return null;
    }

    /**
     * Fetch details for property
     * @param string $url
     * @return |null
     */
    protected function getDetails(string $url)
    {
        //printf("3 getDetails" . PHP_EOL . $url . PHP_EOL);
        try {
            if ($url) {
                $response = $this->client->post($url);
                if ($response && $response->getBody()) {
                    $data = $response->getBody()->getContents();
                    return $this->parseDetails($data);
                }
            }
        }catch (\Exception $e) {
            Log::critical($e);
        }
        return null;
    }

    /**
     * Try to find units inside HTML
     * @param $data
     * @return |null
     */
    protected function parseUnits($data) {
        try {
            $html = html_entity_decode(trim($data));
            $html = remove_non_html_tags($html);
            $this->buildingHtml = $html;
            //printf('parseUnits'.PHP_EOL);
            $this->prepareDataFormHTMLNew($html);
            preg_match_all('/<script id="__NEXT_DATA__" type="application\/json">(.*?)<\/script>/', $html, $match);
            if(!(isset($match[1]) && isset($match[1][0]))){
                Log::error('getUnits: SKIPPED (__NEXT_DATA__ script not found) : ');
                return null;
            }
            $res = json_decode($match[1][0], false);

            $this->buildingData = $res->props->initialData->building;
            return $res->props->initialData->building;
        } catch (\Exception $e) {
            Log::critical($e);
            return null;
        }
        return null;
    }

    /**
     * Try to find property additional information inside HTML
     * @param $data
     * @return |null
     */
    protected function parseDetails($data) {
        try {
            $html = html_entity_decode(trim($data));
            $html = remove_non_html_tags($html);
            //printf('parseDetails'.PHP_EOL);
            $this->prepareDataFormHTMLNew($html);
            $this->buildingHtml = $html;
            preg_match_all('/<script id=hdpApolloPreloadedData type="application\/json">(.*?)<\/script>/', $html, $match);

            if(!(isset($match[1]) && isset($match[1][0]))){
                Log::error("parseDetails: SKIPPED");
                return $this->backupBuildingInfo;
            }
            $res = json_decode($match[1][0], false);

            if ($res && $res->apiCache) {
                $res = json_decode($res->apiCache, true);
                $keys = array_keys($res);
                Log::debug($res);
                foreach ($keys as $key) {
                    if (str_starts_with($key, 'ForRentDoubleScrollInitialRenderSEOQuery')) {
                        $res = json_decode(json_encode($res[$key]), false);

                        if ($res && $res->property) {
                            return $res->property;
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            Log::critical($e);
            dd('Exception');
            return $this->backupBuildingInfo;
        }
        return $this->backupBuildingInfo;
    }

    /**
     * Reset properties pagination
     */
    private function resetPagination() {
        $this->totalPages = 1;
        $this->currentPage = 1;
        $this->totalResults = 0;
        $this->perPage = 0;
    }

    /**
     * Split region to smaller peaces
     * @param $url
     * @return bool|void
     */
    private function splitRegion($url) {
        Log::info("SPLITTING");
        $parts = parse_url($url);
        $params = null;
        parse_str($parts['query'], $params);
        if (isset($params['searchQueryState'])) {
            $params = json_decode($params['searchQueryState'], false);
            if (!isset($params->mapBounds)) {
                Log::error('CAN\'t FIND MAP BOUNDS');
                return;
            }
            $map = $params->mapBounds;
            $west = $map->west;
            $east = $map->east;
            $south = $map->south;
            $north = $map->north;
            $xMiddle = $west + ($east - $west)/2;
            $yMiddle = $south + ($north - $south)/2;
            $regions = [];

            //top left
            $params->mapBounds->west = $west;
            $params->mapBounds->north = $north;
            $params->mapBounds->south = $yMiddle;
            $params->mapBounds->east = $xMiddle;
            $regions[] = $this->baseUrl . json_encode($params);

            //top right
            $params->mapBounds->west = $xMiddle;
            $params->mapBounds->north = $north;
            $params->mapBounds->south = $yMiddle;
            $params->mapBounds->east = $east;
            $regions[] = $this->baseUrl . json_encode($params);

            //bottom left
            $params->mapBounds->west = $west;
            $params->mapBounds->north = $yMiddle;
            $params->mapBounds->south = $south;
            $params->mapBounds->east = $xMiddle;
            $regions[] = $this->baseUrl . json_encode($params);

            //bottom right
            $params->mapBounds->west = $xMiddle;
            $params->mapBounds->north = $yMiddle;
            $params->mapBounds->south = $south;
            $params->mapBounds->east = $east;
            $regions[] = $this->baseUrl . json_encode($params);

            foreach($regions as $region) {
                $this->getPropertiesForRent($region, true);
            }
            return true;
        }
        Log::error('INVALID URL FOR SPLITTING: '. $url);
    }

    /**
     * Process property units
     * @param $item
     * @param $region
     */
    private function processUnits($item, $region) {
        //$item->detailUrl = "https://www.zillow.com/b/sycamore-commons-apartments-fremont-ca-5XjMv8/";
        $units = $this->getUnits($item->detailUrl);
        //dd($this->backupBuildingInfo);
        if ($units && $units->floorPlans && is_array($units->floorPlans) ) {
            foreach ($units->floorPlans as $floorPlan) {
                try {
                    if ($floorPlan->units) {
                        foreach ($floorPlan->units as $unit) {
                            $unit->beds = $floorPlan->beds;
                            $unit->baths = $floorPlan->baths;
                            $unit->sqft = $floorPlan->sqft;
                            $this->saveProperty($item, $region, $unit);
                        }
                    }
                }
                catch (\Exception $e){
                    Log::critical($e);
                }

            }
        } else if($units && $units->ungroupedUnits && is_array($units->ungroupedUnits)) {
            $ungroupedUnits = $units->ungroupedUnits;
            foreach ($ungroupedUnits as $unit) {
                try {
                    if ($unit->listingType === 'FOR_RENT') {
                        $this->saveProperty($item, $region, $unit);
                    }
                }
                catch (\Exception $e){
                    Log::critical($e);
                }
            }
        }
    }

    /**
     * Detect property type and call appropriate method
     * @param $item
     * @param $region
     */
    private function processProperty($item, $region)
    {
        if ($item->detailUrl) {
            if (isset($item->isBuilding) && $item->isBuilding) {
                printf("Building with detailUrl! $item->zpid" . PHP_EOL);
                Log::info("Building with detailUrl! $item->zpid");
                $this->processUnits($item, $region);
                //dd('END BUILDING');
            } else {
                //printf("PRVI " . PHP_EOL);
                //$item->detailUrl = "https://www.zillow.com//homedetails/39495-Albany-Cmn-APT-G-Fremont-CA-94538/25019470_zpid/";
                printf("House with detailUrl! $item->zpid" . PHP_EOL);
                Log::info("House with detailUrl! $item->zpid");
                //dd($this->getDetails($item->detailUrl));
                $this->saveProperty($item, $region, null, $this->getDetails($item->detailUrl));
            }
        } else {
            printf("House without detailUrl! $item->zpid" . PHP_EOL);
            Log::info("House without detailUrl! $item->zpid");
            $this->saveProperty($item, $region);
        }
    }

    /**
     * Try to find some valuable information inside building facts
     *
     * @param $facts
     * @param $data
     * @return mixed
     */
    private function setAdditionalFacts($facts, $data) {
        if (isset($facts) && is_array($facts)) {
            $keys = array_keys($data);
            foreach ($facts as $fact) {
                $key = strtolower($fact->factLabel);
                $value = $fact->factValue;
                if (in_array($key, $keys) && is_null($data[$key]) && !is_null($value)) {
                    $data[$key] = $value;
                    //printf("SET: $key TO $value");
                }
            }
        }
        return $data;
    }

    /**
     * Populate simple props for property/unit
     *
     * @param $facts
     * @param $data
     * @return mixed
     */
    private function setSimpleProps($facts, $data, $props = [], $override = true)
    {
        $simpleProps = is_array($props) && count($props) > 0 ? $props : ['heating', 'cooling', 'parking', 'laundry'];
        foreach ($simpleProps as $prop) {
            if (isset($facts->{$prop})) {
                if (!$override && is_null($data[$prop])) {
                    $data[$prop] = is_array($facts->{$prop}) ? implode(', ',
                        $facts->{$prop}) : $facts->{$prop};
                }
            }
        }
        $data = isset($facts->atAGlanceFacts) ? $this->setAdditionalFacts($facts->atAGlanceFacts, $data) : $data;
        return $data;
    }

    /**
     * Create unique response based on combination of unit/building information
     *
     * @param $propertyUrl
     * @param $buildingDetails
     * @return array|mixed
     */
    private function parseBuildingDetails($propertyUrl, $buildingDetails, $fetchAgain = false) {

        $details = null;
        if ($fetchAgain) {
            $details = $this->getDetails($propertyUrl);
        } else {
            //printf("2 $propertyUrl" . PHP_EOL);
            $details = $this->parseUnits($this->buildingHtml);
        }
        $result = $this->backupBuildingInfo;
        //$details = $this->getDetails($propertyUrl);
        //$details = $this->parseDetails($this->buildingHtml);

        if (!is_null($details)) {

            $result['beds'] = isset($details->bedrooms) ? $details->bedrooms : $result['beds'];
            $result['baths'] = isset($details->bathrooms) ? $details->bathrooms : $result['baths'];
            $result['price'] = isset($details->price) ? $details->price : $result['price'];
            $result['img'] = isset($details->desktopWebHdpImageLink) ? $details->desktopWebHdpImageLink : $result['img'];
            if (isset($details->resoFacts)) {
                $facts = $details->resoFacts;
                $result['type'] = isset($facts->homeType) ? $facts->homeType : $result['type'];
                $result['year_built'] = isset($facts->yearBuilt) ? $facts->yearBuilt :  $result['year_built'];
                if (isset($facts->onMarketDate)) {
                    $result['listed_at'] = Carbon::createFromTimestampMs($facts->onMarketDate);
                }
                $result = $this->setSimpleProps($facts, $result, ['heating', 'cooling', 'parking', 'laundryFeatures']);
            }
            if(isset($details->homeFacts)){
                $result = isset($details->homeFacts->atAGlanceFacts) ? $this->setAdditionalFacts($details->homeFacts->atAGlanceFacts, $result) : $result;
            }
        }

        if (!is_null($buildingDetails) && isset($buildingDetails->amenitySummary)) {
            $buildingFacts = $buildingDetails->amenitySummary;
            if (is_null($result['year_built']) && isset($buildingFacts->buildingDetails) && is_array($buildingFacts->buildingDetails)) {
                foreach ($buildingFacts->buildingDetails as $buildingDetail) {
                    if (strpos($buildingDetail, "built in ") !== false) {
                        $result['year_built'] = explode('built in ', $buildingDetail)[1];
                    }
                }
            } else if (is_null($result['year_built']) && isset($buildingFacts->buildingDetails) && strpos($buildingFacts->buildingDetails, "built in ") !== false) {
                $result['year_built'] = explode('built in ', $buildingFacts->buildingDetails)[1];
            }
            $result = $this->setSimpleProps($buildingFacts, $result, false);
        }
        return $result;
    }

    /**
     * Set property attributes based on unit/building information
     *
     * @param Property $property
     * @param $details
     */
    private function setDetails(Property $property, $baseDetails, $fetchAgain = false) {
        $details = $this->parseBuildingDetails($property->url, $baseDetails, $fetchAgain);
        $property->sqft = $details['sqft'] ?: $property->sqft;
        $property->beds = $details['beds'] ?: $property->beds;
        $property->baths = $details['baths'] ?: $property->baths;
        $property->price = $details['price'] ?: $property->price;
        $property->img = $details['img'] ?: $property->img;
        $property->type = $details['type'] ?: $property->type;
        $property->year_built = $details['year_built'] ?: $property->year_built;
        $property->listed_at = $details['listed_at'];
        $property->heating = $details['heating'];
        $property->cooling = $details['cooling'];
        $property->parking = $details['parking'];
        $property->laundry = $details['laundry'];
    }

    /**
     * Save property to DB based on input parameters
     * @param $item
     * @param Region $region
     * @param null $unit
     * @param null $details
     * @return mixed
     */
    private function saveProperty($item, Region $region, $unit = null, $details = null) {
        $zpid = $unit ? $unit->zpid : $item->zpid;

        $property = Property::firstOrNew(['zpid' => $zpid]);//, 'region_id' => $region->id

        $property->region_id = $region->id;
        $property->address = isset($item->addressStreet) ? $item->addressStreet : null;
        $property->state = isset($item->addressState) ? $item->addressState : null;
        $property->city = isset($item->addressCity) ? $item->addressCity : null;
        $property->name = isset($item->isBuilding) && $item->isBuilding ? $item->buildingName : null;
        $property->url = isset($item->detailUrl) ? $item->detailUrl : null;
        $property->img = isset($item->imgSrc) ? $item->imgSrc : null;
        $property->sqft = isset($item->area) ? $item->area : null;
        $property->price = isset($item->price) ? $item->price : null;
        $property->last_seen_at = Carbon::now();
        $property->first_seen_at = !$property->id ? Carbon::now() : $property->first_seen_at;
        if ($details) {
            $this->setDetails($property, $details);
        }

        if ($unit) {
            //$unit->hdpUrl = "/homedetails/1452-Bay-St-UNIT-B-Alameda-CA-94501/2077846981_zpid/";
            $property->sqft = isset($unit->sqft) ? $unit->sqft : $property->sqft;
            $property->price = isset($unit->price) ? $unit->price : $property->price;
            $property->beds = isset($unit->beds) ? $unit->beds : null;
            $property->baths = isset($unit->baths) ? $unit->baths : null;
            $property->unit_number = isset($unit->unitNumber) ? $unit->unitNumber : null;
            if (isset($unit->hdpUrl) && $unit->hdpUrl && !$details) {
                $property->url = config('zillow.base_url') . $unit->hdpUrl;
                $this->setDetails($property, $item, true);
            } else {
                $this->setDetails($property, $this->buildingData);
            }
        }

        $property->lat = isset($item->latLong) && isset($item->latLong->latitude) ? $item->latLong->latitude : null;
        $property->lng = isset($item->latLong) && isset($item->latLong->longitude) ? $item->latLong->longitude : null;
        $dirty = $property->isDirty('price');
        $property->save();
        if($dirty){
            $price = new PropertyPrice;
            $price->price = $property->price;
            $price->property()->associate($property);
            $price->save();
        }

        return $property;
    }

    /**
     * Add random delay between two calls
     */
    private function delay($force = false) {
        if (!$force && config('app.debug')) {
            return;
        }
        $minDelay = config('zillow.min_seconds_delay');
        $maxDelay = config('zillow.max_seconds_delay');
        if ($maxDelay < 1 || $minDelay >= $maxDelay) {
            return;
        }
        $delay = rand($minDelay, $maxDelay);
        Log::info("Sleeping for $delay seconds!");
        sleep($delay);
    }

    /**
     * Fetch valuable information from HTML
     * @param $html
     * @return null
     */
    private function prepareDataFormHTML($html) {
        $data = $this->defaultBuildingInfo;
        preg_match_all('/<li class="ds-home-fact-list-item">(.*?)<\/li>/', $html, $match);
        if(!isset($match[1]) || count($match[1]) == 0){
            Log::error('ADDITIONAL HTML DATA 1 NOT FOUND');
            preg_match_all('/<div aria-hidden="true" class="Flex-n94bjd-0 sc-fzonZV hEGsFw">(.*?)<\/li>/', $html, $match);
            if( !isset($match[1]) || count($match[1]) == 0){
                Log::error('ADDITIONAL HTML DATA 2 NOT FOUND');
            } else {
                $facts = $match[1];
                if (is_array($facts)) {
                    $keys = array_keys($data);
                    foreach ($facts as $fact) {
                        preg_match('/<title>(.*?)<\/title>/', $fact, $match);
                        $key = !isset($match[1]) ? null : strtolower($match[1]);
                        if (!in_array($key, $keys)) {
                            preg_match('/<label data-test-id="title" class="Text-aiai24-0 fGEVQh">(.*?)<\/label>/', $fact, $match);
                            $key = !isset($match[1]) ? null : strtolower(str_replace('<!-- -->:', '', trim($match[1]," \t\n\r\0\x0B\xC2\xA0")));
                        }
                        preg_match('/<span data-test-id="desc" class="Text-aiai24-0 bolUta">(.*?)<\/span>/', $fact, $match);
                        $value = !isset($match[1]) ? null : $match[1];
                        if (in_array($key, $keys) && is_null($data[$key]) && !is_null($value)) {
                            $data[$key] = $value;
                        }
                    }
                }
            }
        } else {
            //FORMAT OF DATA
            //Date available:</span><span class="ds-body ds-home-fact-value">Contact manager</span>
            //Type:</span><span class="ds-body ds-home-fact-value">Apartment</span>
            $facts = $match[1];
            if (is_array($facts)) {
                $keys = array_keys($data);
                foreach ($facts as $fact) {
                    preg_match('/<span class="ds-standard-label ds-home-fact-label">(.*?)<\/span>/', $fact, $match);
                    $key = !isset($match[1]) ? null : strtolower(str_replace(':', '', $match[1]));
                    preg_match('/<span class="ds-body ds-home-fact-value">(.*?)<\/span>/', $fact, $match);
                    $value = !isset($match[1]) ? null : $match[1];
                    if (in_array($key, $keys) && is_null($data[$key]) && !is_null($value)) {
                        $data[$key] = $value;
                    }
                }
            }
        }

        preg_match('/<div class="ds-summary-row">(.*?)<\/div>/', $html, $summary);
        //dd($summary);

        if(isset($summary[1]) && !is_null($summary[1])){
            preg_match('/<span class="ds-value">(.*?)<\/span>/', $summary[1], $price);
            preg_match_all('/<span class="ds-bed-bath-living-area">(.*?)<\/span><\/span>/', $summary[1], $basicInfo);

            if( isset($price[1]) && !is_null($price[1])) {
                //$data['price'] = str_replace(',', '', (str_replace('$', '', $price[1])));
                $data['price'] = preg_replace("/[^0-9.]/", "", $price[1]);
            }
            if( isset($basicInfo[0]) && !is_null($basicInfo[0])) {
                foreach ($basicInfo[0] as $info) {
                    preg_match('/<span>(.*?)<\/span>/', $info, $value);
                    preg_match('/class="ds-summary-row-label-secondary"><!-- -->(.*?)<\/span>/', $info, $key);
                    if(isset($value[1]) && isset($key[1]) && $value[1] != "--"){
                        $vl = preg_replace('#[^\w()/.%\-&]#',"",$value[1]);
                        if ($key[1] == 'bd') {
                            $data['beds'] = $vl;
                        }
                        if ($key[1] == 'ba') {
                            $data['baths'] = $vl;
                        }
                        if ($key[1] == 'sqft') {
                            $data['sqft'] = $vl;
                        }
                    }
                }
                //$data['price'] = str_replace('$', '', $basicInfo);
            }
        }
        $this->backupBuildingInfo = $data;
    }

    /**
     * Fetch valuable information from HTML
     * @param $html
     * @return null
     */
    private function prepareDataFormHTMLNew($html) {
        $data = $this->defaultBuildingInfo;
        preg_match_all('/<li class="ds-home-fact-list-item">(.*?)<\/li>/', $html, $match);
        if(!isset($match[1]) || count($match[1]) == 0){
            //$match, $html, 'OVDE2');
            Log::error('ADDITIONAL HTML DATA 1 NOT FOUND');
            preg_match_all('/<div aria-hidden="true" class="Flex-n94bjd-0 sc-fzonZV hEGsFw">(.*?)<\/li>/', $html, $match);
            if( !isset($match[1]) || count($match[1]) == 0){
                Log::error('ADDITIONAL HTML DATA 2 NOT FOUND');
            } else {
                $facts = $match[1];
                if (is_array($facts)) {
                    $keys = array_keys($data);
                    foreach ($facts as $fact) {
                        preg_match('/<title>(.*?)<\/title>/', $fact, $match);
                        $key = !isset($match[1]) ? null : strtolower($match[1]);
                        if (!in_array($key, $keys)) {
                            preg_match('/<label data-test-id="title" class="Text-aiai24-0 fGEVQh">(.*?)<\/label>/', $fact, $match);
                            $key = !isset($match[1]) ? null : strtolower(str_replace('<!-- -->:', '', trim($match[1]," \t\n\r\0\x0B\xC2\xA0")));
                        }
                        preg_match('/<span data-test-id="desc" class="Text-aiai24-0 bolUta">(.*?)<\/span>/', $fact, $match);
                        $value = !isset($match[1]) ? null : $match[1];
                        if (in_array($key, $keys) && is_null($data[$key]) && !is_null($value)) {
                            $data[$key] = $value;
                        }
                    }
                }
            }
        } else {
            //FORMAT OF DATA
            //Type:</span><span class="Text-c11n-8-33-0__aiai24-0 sc-oTaid gJQyqC">SingleFamily</span>
            $facts = $match[1];
            if (is_array($facts)) {
                $keys = array_keys($data);
                foreach ($facts as $fact) {
                    foreach ($keys as $key) {
                        $label = ucfirst($key);
                        $label = str_replace('_', ' ', $label);
                        $pos = strpos($fact, $label);
                        if ($pos !== false) {
                            $dirtyValue = substr($fact, $pos,-7);//to escape </span>
                            $revertValue = strrev($dirtyValue);
                            $spanEndPos = strpos($revertValue, '>');
                            if ($spanEndPos !== false) {
                                $dirtyValue = substr($revertValue, 0, $spanEndPos);
                                $value = strrev($dirtyValue);
                               if (is_null($data[$key]) && !is_null($value) && $value != "--") {
                                    $data[$key] = $value;
                               }
                            }

                        }

                    }
                }
            }
        }

        preg_match('/<div class="ds-summary-row">(.*?)<\/div>/', $html, $summary);
        if(!isset($summary[1]) || is_null($summary[1])) {
            preg_match('/<div class="ds-bed-bath-living-area-header">(.*?)<\/div>/', $html, $summary);
        }
        //dd(isset($summary[1]) && !is_null($summary[1]));
        if(isset($summary[1]) && !is_null($summary[1])){
            preg_match_all('/<span(.*?)<\/span>/', $summary[1], $basicInfo);
            $basicInfoArray = [];
            if( isset($basicInfo[1]) && !is_null($basicInfo[1])) {
                foreach ($basicInfo[1] as $info) {
                    $revertValue = strrev($info);
                    $spanEndPos = strpos($revertValue, '>');
                    $dirtyValue = substr($revertValue, 0, $spanEndPos);
                    $value = strrev($dirtyValue);
                    if ($value)  {
                        $basicInfoArray[] = $value;
                    }
                }
                for ($i = 0;$i < count($basicInfoArray); $i++){
                    if ($i == 0 && strpos($basicInfoArray[$i], '$') !== false) {
                        if (is_null($data['price']) && !is_null($basicInfoArray[$i])) {
                            $data['price'] = $basicInfoArray[$i];
                            if (strpos($basicInfoArray[$i+1], '/') !== false) {
                                $data['price'] .= $basicInfoArray[$i+1];
                                $i++;
                            }
                        }
                    }
                    if (isset($basicInfoArray[$i+1])) {
                        if ($basicInfoArray[$i+1] == 'bd') {
                            $data['beds'] = $basicInfoArray[$i] != "--" ? $basicInfoArray[$i] : null;
                        }
                        if ($basicInfoArray[$i+1] == 'ba') {
                            $data['baths'] = $basicInfoArray[$i] != "--" ? $basicInfoArray[$i] : null;
                        }
                        if ($basicInfoArray[$i+1] == 'sqft') {
                            $data['sqft'] = $basicInfoArray[$i] != "--" ? $basicInfoArray[$i] : null;
                        }
                    }
                }
            }
        }
        $this->backupBuildingInfo = $data;
    }

}
