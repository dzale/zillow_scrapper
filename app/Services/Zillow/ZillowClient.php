<?php

namespace App\Services\Zillow;

use App\Services\Common\Guzzle\ScraperClient;
use App\Support\ProxyList;
use App\Support\UserAgent;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Str;

class ZillowClient extends ScraperClient
{
    /**
     * Initialize HTTP client adapted to zillow service
     * @return Client
     */
    protected function initClient() {
        $client =  new Client([
            'cookies' => new CookieJar(),
            'headers' => [
                'accept'           => '*/*',
                'accept-encoding'  => 'gzip, deflate, br',
                'accept-language'  => 'en,en-US;q=0.8,ru;q=0.6,sr;q=0.4,uk;q=0.2',
                'cache-control'    => 'no-cache',
                'dnt'              => 1,
                'pragma'           => 'no-cache',
                'user-agent'       => UserAgent::get(),
                'x-requested-with' => 'XMLHttpRequest',
                "Authority"        => "www.zillow.com",
//                "sec-ch-ua"        => '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
//                "sec-fetch-site"   => 'same-origin',
//                "sec-fetch-mode"   => 'cors',
//                "sec-fetch-dest"   => 'empty',
                'proxy'            => ProxyList::get(),
                Str::random(5) . "-token" => Str::random(32),
            ],
            'timeout'         => 300,
            'connect_timeout' => 300,
        ]);
        printf(ProxyList::get() . PHP_EOL);
        return $client;
    }
}
