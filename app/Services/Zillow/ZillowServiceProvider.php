<?php

namespace App\Services\Zillow;

use Illuminate\Support\ServiceProvider;

class ZillowServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        return [Zillow::class];
    }
}
