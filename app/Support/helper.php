<?php
/**
 * Remove the non html tags which might cause problems
 * @param $html
 * @return mixed
 */
function remove_non_html_tags($html)
{
    $html = preg_replace('~>\\s+<~m', '><', $html);
    $html = str_replace("<nobr>", "", $html);
    $html = str_replace("</nobr>", "", $html);
    $html = str_replace("&", '&amp;', $html);
    $html = str_replace("&nbsp;", '', $html);
    $html = str_replace("\r", "", $html);
    $html = str_replace("\n", "", $html);
    $html = str_replace("\r\n", "", $html);
    $html = str_replace("\t", "", $html);
    if (!str_contains(strtolower($html), 'charset=utf-8')) {
        $html = '<?xml encoding="utf-8" ?>' . $html;
    }
    return $html;
}

/**
 * Delete all backups in directory $dir older then 5 $max_age
 * @param $dir
 * @param $max_age
 * @return array|void
 */
function delete_older_than($dir, $max_age) {
    $list = array();

    $limit = time() - $max_age;

    $dir = realpath($dir);

    if (!is_dir($dir)) {
        return;
    }

    $dh = opendir($dir);
    if ($dh === false) {
        return;
    }

    while (($file = readdir($dh)) !== false) {
        $file = $dir . '/' . $file;
        if (!is_file($file)) {
            continue;
        }

        if (filemtime($file) < $limit) {
            $list[] = $file;
            unlink($file);
        }

    }
    closedir($dh);
    return $list;
}

/**
 * Delete all backups older then 5 days
 */
function delete_old_backups(){
    delete_older_than(storage_path('backups'), 3600*24*5);
}
