<?php

namespace App\Tables;

use App\Models\Property;
use Okipa\LaravelTable\Abstracts\AbstractTable;
use Okipa\LaravelTable\Table;

class PropertiesTable extends AbstractTable
{
    /**
     * Configure the table itself.
     *
     * @return \Okipa\LaravelTable\Table
     * @throws \ErrorException
     */
    protected function table(): Table
    {
        return (new Table)->model(Property::class)
            ->identifier('properties-table')
            ->routes([
                'index'   => ['name' => 'properties.index']
            ]);
    }

    /**
     * Configure the table columns.
     *
     * @param \Okipa\LaravelTable\Table $table
     *
     * @throws \ErrorException
     */
    protected function columns(Table $table): void
    {
        $table->column('id')->title(__('property.id'))->html(fn(Property $property) => $this->prepareId($property))->sortable(true);
        $table->column('image')->title(__('property.image'))->html(fn(Property $property) => $this->prepareImage($property));
        $table->column('address')->title(__('property.address'))->html(fn(Property $property) => $this->prepareMap($property))->sortable()->searchable();
        $table->column('region_id')->title(__('property.region'))->html(fn(Property $property) => $property->region->name)->sortable()->searchable();
/*        $table->column('type')->title(__('property.type'))->sortable();*/
        //$table->column('name')->title(__('property.name'))->sortable()->searchable();
        $table->column('unit_number')->title(__('property.unit_number'))->sortable()->searchable();
        $table->column('sqft')->title(__('property.sqft'))->sortable();
        $table->column('price')->title(__('property.price'))->sortable()->searchable();
        $table->column('beds')->title(__('property.beds'))->sortable();
        $table->column('baths')->title(__('property.baths'))->sortable();
/*        $table->column('heating')->title(__('property.heating'))->sortable();
        $table->column('cooling')->title(__('property.cooling'))->sortable();
        $table->column('laundry')->title(__('property.laundry'))->sortable();
        $table->column('parking')->title(__('property.parking'))->sortable();
        $table->column('year_built')->title(__('property.year_built'))->sortable();*/
/*        $table->column('first_seen_at')->title(__('property.first_seen_at'))->dateTimeFormat('d/m H:i')->sortable();*/
        $table->column('last_seen_at')->title(__('property.last_seen_at'))->dateTimeFormat('d/m H:i')->sortable();
/*        $table->column('listed_at')->title(__('property.listed_at'))->dateTimeFormat('d/m H:i')->sortable();*/
    }

    /**
     * Configure the table result lines.
     *
     * @param \Okipa\LaravelTable\Table $table
     */
    protected function resultLines(Table $table): void
    {
        //
    }

    function prepareId(Property $property): string
    {
        return
            '<div class="text-center"><a href="/' . $property->id . '" target="_blank">' . $property->id . '</a></div>';
    }

    function prepareMap(Property $property): string
    {
        return
            '<a href="https://www.google.com/maps/search/?api=1&query=' . $property->lat .',' . $property->lng . '" target="_blank">' . $property->address . '</a>';
    }

    function prepareImage(Property $property): string
    {
        return
            '<a href="javascript:void(0)" class="property-image" data-url="' . $property->url . '">' .
                '<img height="50" src="' . ($property->img ?: url('images/no-image.jpg')).'" alt="' . $property->name . '">' .
            '</a>';
    }
}
