<?php

return [

    'base_url' => env('ZILLOW_BASE_URL', 'https://www.zillow.com/'),
    //'search_url' => env('ZILLOW_SEARCH_URL', 'https://www.zillow.com/search/GetSearchPageState.htm?searchQueryState='),
    //'search_url' => env('ZILLOW_SEARCH_URL', 'https://www.zillow.com/search/GetSearchPageState.htm?searchQueryState='),
    'search_url' => env('ZILLOW_SEARCH_URL', 'https://www.zillow.com/homes/for_rent/?searchQueryState='),
    'min_seconds_delay' => env('ZILLOW_MIN_SECONDS_DELAY', 5),
    'max_seconds_delay' => env('ZILLOW_MAX_SECONDS_DELAY', 10)
];
