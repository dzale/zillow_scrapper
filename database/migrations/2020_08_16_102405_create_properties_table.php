<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->string('zpid')->nullable();
            $table->unsignedBigInteger('region_id');
            $table->longText('url')->nullable();
            $table->longText('img')->nullable();
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('unit_number')->nullable();
            $table->double('beds', 8, 2)->nullable();
            $table->double('baths', 8, 2)->nullable();
            $table->string('type')->nullable();
            $table->string('cooling')->nullable();
            $table->string('heating')->nullable();
            $table->string('parking')->nullable();
            $table->unsignedInteger('year_built')->nullable();
            $table->string('laundry')->nullable();
            $table->string('price')->nullable();
            $table->string('sqft')->nullable();
            $table->decimal('lat', 11,8)->nullable();
            $table->decimal('lng', 11,8)->nullable();
            $table->timestamp('listed_at')->nullable();
            $table->timestamp('first_seen_at')->nullable();
            $table->timestamp('last_seen_at')->nullable();
            $table->timestamps();

            $table->foreign('region_id')
                ->references('id')
                ->on('regions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropForeign(['region_id']);
        });
        Schema::dropIfExists('properties');
    }
}
