require('./bootstrap');
import Swal from '../../node_modules/sweetalert2/dist/sweetalert2.all.min'
$( document ).ready(function() {
    $('.property-image, .property-address').click(function () {
        let url = $(this).data('url');
        var input = document.body.appendChild(document.createElement("input"));
        input.value = url;
        input.focus();
        input.select();
        document.execCommand('copy');
        input.parentNode.removeChild(input);
        Swal.fire('Property URL has been copied!');
    });
});