<?php

return [

    'na' => 'N/A',
    'id' => 'ID',
    'name' => 'Name',
    'date' => 'Date',
    'created_at' => 'Created',
    'updated_at' => 'Updated',
    'address' => 'Address',
    'price' => 'Price',
    'image' => 'Image',
    'sqft' => 'Sqft',
    'unit_number' => 'Unit #',
    'region' => 'Region',
    'beds' => 'Beds',
    'baths' => 'Bathrooms',
    'type' => 'Type',
    'heating' => 'Heating',
    'location' => 'Location',
    'cooling' => 'Cooling',
    'laundry' => 'Laundry',
    'parking' => 'Parking',
    'year_built' => 'Year built',
    'listed_at' => 'Listed on Zillow',
    'first_seen_at' => 'First seen',
    'last_seen_at' => 'Last seen',
    'contact_manager' => 'Contact manager',
    'price_history' => 'Price history',
    'f_and_f' => 'Facts and features',
];
