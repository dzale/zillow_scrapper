<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
        <link rel="stylesheet" href="css/app.css">

        <title>Zillow scraper</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <a href="javascript:void(0)" class="property-address" data-url="{{ $property->url }}">
                        <h1>{{$property->address}}</h1>
                    </a>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-lg-3 col-sm-6 mb-4">
                    <a href="'{{ $property->url }}" target="_blank">
                        <img src="{{($property->img ?: url('/images/no-image.jpg')) }}" alt="{{ $property->name }}" style="max-width: 100%">
                    </a>
                </div>
                <div class="col-lg-9 col-sm-6">
                    <div class="mb-1"><b class="mr-2">{{__('property.name')}}: </b> {{ $property->name ?: __('property.na')}}</div>
                    @if($property->year_built)
                        <div class="mb-1"><b class="mr-2">{{__('property.year_built')}}: </b> {{ $property->year_built ?: __('property.na')}}</div>
                    @endif
                    @if($property->unit_number)
                        <div class="mb-1"><b class="mr-2">{{__('property.unit_number')}}: </b> {{ $property->unit_number ?: __('property.na')}}</div>
                    @endif
                    <div class="mb-1"><b class="mr-2">{{__('property.sqft')}}: </b> {{ $property->sqft ?: __('property.na')}}</div>
                    <div class="mb-1"><b class="mr-2">{{__('property.price')}}: </b> {{ $property->price ?: __('property.na')}}</div>
                    <div class="mb-4">
                        <span><i class="fas fa-globe-americas mr-2"></i> <b class="mr-2">{{__('property.location')}}: </b></span>
                        <a href="https://www.google.com/maps/search/?api=1&query={{ $property->lat .',' . $property->lng }}" target="_blank">Map</a>
                    </div>
                </div>
            </div>
            <div class="row mb-4">
                <div class="col-lg-3 col-sm-6 mb-4">
                    <h4>{{__('property.price_history')}}:</h4>
                    @if($property->prices)
                        <div class="row mb-1 price-title">
                            <div class="col-7">
                                <b>{{__('property.date')}}</b>
                            </div>
                            <div class="col-5">
                                <b>{{__('property.price')}}</b>
                            </div>
                        </div>
                    @endif
                    @foreach($property->prices as $key => $price)
                        @if($key === 0 || (!isset($property->prices[$key+1]) && (count($property->prices) > 2 || (isset($property->prices[$key-1]) && $property->prices[$key-1]->normalized_price != $price->normalized_price))) || (isset($property->prices[$key+1]) && $property->prices[$key+1]->normalized_price != $price->normalized_price))
                            <div class="row mb-1">
                                <div class="col-7">
                                    {{$price->date}}
                                </div>
                                <div class="col-5">
                                    <span class="float-right">{{$price->normalized_price}}
                                        @if(isset($property->prices[$key+1]))
                                            <i class="fas fa-{{$price->normalized_price > $property->prices[$key+1]->normalized_price ? 'arrow-up' : ($price->normalized_price < $property->prices[$key+1]->normalized_price ? 'arrow-down' : 'minus')}} ml-1"></i>
                                        @else
                                            <i class="fas fa-minus ml-1"></i>
                                        @endif
                                    </span>
                                </div>
                            </div>
                        @endif
                    @endforeach

                </div>
                <div class="col-lg-9 col-sm-6 mb-4">
                    <h4>{{__('property.f_and_f')}}:</h4>
                    <div class="row">
                        <div class="col-lg-4 col-sm-6">
                            <div class="row mb-1">
                                <div class="col-12">
                                    <span><i class="fas fa-bed mr-2"></i> <b class="mr-1">{{__('property.beds')}}:</b></span> <span>{{$property->beds ?: __('property.na')}}</span>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <span><i class="fas fa-hotel mr-2"></i> <b class="mr-1">{{__('property.type')}}:</b></span> <span>{{$property->type ?: __('property.contact_manager')}}</span>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <span><i class="fas fa-snowflake mr-2"></i> <b class="mr-1">{{__('property.cooling')}}:</b></span> <span>{{$property->cooling ?: __('property.contact_manager')}}</span>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <span><i class="fas fa-parking mr-2"></i> <b class="mr-1">{{__('property.parking')}}:</b></span> <span>{{$property->parking ?: __('property.contact_manager')}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="row mb-1">
                                <div class="col-12">
                                    <span><i class="fas fa-bath mr-2"></i> <b class="mr-1">{{__('property.baths')}}:</b></span> <span>{{$property->baths ?: __('property.na')}}</span>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <span><i class="fas fa-soap mr-2"></i> <b class="mr-1">{{__('property.laundry')}}:</b></span> <span>{{$property->laundry ?: __('property.contact_manager')}}</span>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-12">
                                    <span><i class="fas fa-temperature-high mr-2"></i> <b class="mr-1">{{__('property.heating')}}:</b></span> <span>{{$property->heating ?: __('property.contact_manager')}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="js/app.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
    </body>
</html>
